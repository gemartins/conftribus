<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Galeria;
use App\Models\Evento;
use DB;

class WebSiteController extends Controller
{

    public function home()
    {

      $galeria = Galeria::all();

      return view('pages.index',compact('galeria'));
    }
}
